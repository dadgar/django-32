from django.db import models


class Article(models.Model):
    title = models.CharField(max_length=64)
    description = models.TextField()

    def __str__(self):
        return self.title
    
    class Meta:
        verbose_name = 'مقاله'
        verbose_name_plural = 'مقاله ها'